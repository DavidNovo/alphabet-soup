package net.novogrodsky;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParseInputFileTest {

    ParseInputFile objectUnderTest;

    @BeforeEach
    void setUp() {
        objectUnderTest = new ParseInputFile();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void parseFileUsingSmallTestFile() {
        String filename = "src/test/resources/smallTest.txt";
        objectUnderTest.parseFile(filename);

        // Checking the row and column values from the input file
        assertEquals(3, objectUnderTest.getRows(), "The rows should be 3.");
        assertEquals(3, objectUnderTest.getColumns(), "The columns should be 3.");

        // Checking the constructed matrix from the input file
        System.out.println(objectUnderTest.getMatrix().toString());
        assertTrue(objectUnderTest.getMatrix().toString().contains("[A, B, C]"));

        assertFalse(objectUnderTest.getSearchWords().isEmpty());

        // Checking the search words retrieved from the input file
        assertTrue(objectUnderTest.getSearchWords().get(0).contains("ABC"));
        assertTrue(objectUnderTest.getSearchWords().get(1).contains("AEI"));
        assertEquals(2, objectUnderTest.getSearchWords().size());
    }
}