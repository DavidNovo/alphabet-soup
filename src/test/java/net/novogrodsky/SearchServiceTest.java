package net.novogrodsky;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

class SearchServiceTest {

    ParseInputFile parseInputFile;

    @BeforeEach
    void setUp() {
        parseInputFile = new ParseInputFile();
    }

    @Test
    void findWord() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("a", "p", "p"));
        sampleList.add(Arrays.asList("d", "e", "f"));
        sampleList.add(Arrays.asList("g", "h", "i"));

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", false);

        assertEquals(0, results[0], "The word we are looking for is in row 0.");
        assertEquals(0, results[1], "The word we are looking for starts in column 0.");
        assertEquals(0, results[2], "The word we are looking for is in row 0.");
        assertEquals(2, results[3], "The word we are looking for ends in column 2.");
    }

    @Test
    void findWordSpelledBackwardInMatrix() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("p", "p", "a"));
        sampleList.add(Arrays.asList("d", "e", "f"));
        sampleList.add(Arrays.asList("g", "h", "i"));

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", true);

        System.out.print(results[0] + "   " + results[1] + " " + results[2] + " " + results[3]);
        assertEquals(0, results[0], "The word we are looking for is in row 0.");
        assertEquals(2, results[1], "The word we are looking for starts in column 2.");
        assertEquals(0, results[2], "The word we are looking for is in row 0.");
        assertEquals(0, results[3], "The word we are looking for ends in column 1.");
    }

    @Test
    void findWordVertical() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("a", "b", "c"));
        sampleList.add(Arrays.asList("p", "e", "f"));
        sampleList.add(Arrays.asList("p", "h", "i"));

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", false);

        assertEquals(0, results[0], "The word we are looking for starts in row 0.");
        assertEquals(0, results[1], "The word we are looking for is in column 0.");
        assertEquals(2, results[2], "The word we are looking for ends in row 2.");
        assertEquals(0, results[3], "The word we are looking for is in column 0.");
    }

    @Test
    void findWordVerticalLargerMatrix() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("a", "b", "c"));
        sampleList.add(Arrays.asList("p", "e", "f"));
        sampleList.add(Arrays.asList("p", "h", "i"));
        sampleList.add(Arrays.asList("l", "m", "n"));

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", false);

        assertEquals(0, results[0], "The word we are looking for starts in row 0.");
        assertEquals(0, results[1], "The word we are looking for is in column 0.");
        assertEquals(2, results[2], "The word we are looking for ends in row 2.");
        assertEquals(0, results[3], "The word we are looking for is in column 0.");
    }

    @Test
    void findWordVerticalLargerMatrixSpelledBackwardInMatrix() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("p", "b", "c"));
        sampleList.add(Arrays.asList("p", "e", "f"));
        sampleList.add(Arrays.asList("a", "h", "i"));
        sampleList.add(Arrays.asList("l", "m", "n"));

        char[][] matrix = {{'a', 'b', 'c', 'd'},
                {'e', 'f', 'g', 'h'},
                {'i', 'j', 'k', 'l'},
                {'m', 'n', 'o', 'p'}};

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", true);

        assertEquals(2, results[0], "The word we are looking for starts in row 0.");
        assertEquals(0, results[1], "The word we are looking for is in column 0.");
        assertEquals(0, results[2], "The word we are looking for ends in row 2.");
        assertEquals(0, results[3], "The word we are looking for is in column 0.");
    }

    @Test
    void findWordInDiagonalTopLeftToLowerRight() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("a", "b", "c"));
        sampleList.add(Arrays.asList("p", "p", "f"));
        sampleList.add(Arrays.asList("a", "h", "p"));

        char[][] matrix = {{'a', 'b', 'c', 'd'},
                {'e', 'f', 'g', 'h'},
                {'i', 'j', 'k', 'l'},
                {'m', 'n', 'o', 'p'}};

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", false);
        System.out.println(Arrays.toString(results));

        assertEquals(0, results[0], "The word we are looking for starts in row 0.");
        assertEquals(0, results[1], "The word we are looking for starts in column 0.");
        assertEquals(2, results[2], "The word we are looking for ends in row 2.");
        assertEquals(2, results[3], "The word we are looking for is in column 2.");
    }

    @Test
    void findWordInDiagonalBottomLeftToUpperRight() {

        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("x", "b", "p"));
        sampleList.add(Arrays.asList("p", "p", "f"));
        sampleList.add(Arrays.asList("a", "h", "z"));

        char[][] matrix = {{'a', 'b', 'c', 'd'},
                {'e', 'f', 'g', 'h'},
                {'i', 'j', 'k', 'l'},
                {'m', 'n', 'o', 'p'}};

        char[][] testCharMatrix = SearchService.changeToArray(sampleList);
        int[] results = SearchService.findWord(testCharMatrix, "app", false);
        System.out.println(Arrays.toString(results));

        assertEquals(2, results[0], "The word we are looking for starts in row 0.");
        assertEquals(0, results[1], "The word we are looking for starts in column 0.");
        assertEquals(0, results[2], "The word we are looking for ends in row 2.");
        assertEquals(2, results[3], "The word we are looking for is in column 2.");
    }
    @Test
    void findWordInDiagonalBottomLeftToUpperRight2() {

        char[][] matrix = {
                {'a', 'b', 'c', 'd'},
                {'e', 'f', 'g', 'h'},
                {'a', 'j', 'k', 'l'},
                {'i', 'p', 'k', 'l'},
                {'m', 'n', 'p', 'u'}};

        int[] results = SearchService.findWord(matrix, "app", false);
        System.out.println(Arrays.toString(results));

        assertEquals(2, results[0], "The word we are looking for starts in row 0.");
        assertEquals(0, results[1], "The word we are looking for starts in column 0.");
        assertEquals(4, results[2], "The word we are looking for ends in row 2.");
        assertEquals(2, results[3], "The word we are looking for is in column 2.");
    }

    @Test
    void findWordInDiagonalTopLeftToBottomRight3() {

        char[][] matrix = {
                {'a', 'b', 'c', 'd'},
                {'e', 'f', 'g', 'h'},
                {'a', 'j', 'k', 't'},
                {'i', 'p', 'a', 'l'},
                {'m', 'c', 'p', 'u'}};

        int[] results = SearchService.findWord(matrix, "cat", false);
        System.out.println(Arrays.toString(results));

        assertEquals(4, results[0], "The word we are looking for starts in row 0.");
        assertEquals(1, results[1], "The word we are looking for starts in column 0.");
        assertEquals(2, results[2], "The word we are looking for ends in row 2.");
        assertEquals(3, results[3], "The word we are looking for is in column 2.");
    }

    @Test
    void findWordInDiagonalTopLeftToBottomRightBackward3() {

        char[][] matrix = {
                {'a', 'b', 'c', 'd'},
                {'e', 'f', 'g', 'h'},
                {'p', 'j', 'k', 'c'},
                {'i', 'p', 'a', 'l'},
                {'m', 't', 'a', 'u'}};

        int[] results = SearchService.findWord(matrix, "cat", true);
        System.out.println(Arrays.toString(results));

        assertEquals(2, results[0], "The word we are looking for starts in row 0.");
        assertEquals(3, results[1], "The word we are looking for starts in column 0.");
        assertEquals(4, results[2], "The word we are looking for ends in row 2.");
        assertEquals(1, results[3], "The word we are looking for is in column 2.");
    }
    @Test
    void changeToArray() {
        String filename = "src/test/resources/smallTest.txt";
        //parseInputFile.parseFile(filename);
        char[][] sampleArray = {
                {'a', 'b', 'c'},
                {'d', 'e', 'f'},
                {'g', 'h', 'i'}
        };


        List<List<String>> sampleList = new ArrayList<>();
        sampleList.add(Arrays.asList("a", "b", "c"));
        sampleList.add(Arrays.asList("d", "e", "f"));
        sampleList.add(Arrays.asList("g", "h", "i"));


        char[][] testCharMatrix = SearchService.changeToArray(sampleList);

        assertSame(testCharMatrix.getClass().getSimpleName(), sampleArray.getClass().getSimpleName());
        System.out.println(testCharMatrix[0][0]);
        System.out.println(sampleList.get(0).get(0));
        assertEquals((sampleList.get(0).get(0)).charAt(0), testCharMatrix[0][0]);
    }
}