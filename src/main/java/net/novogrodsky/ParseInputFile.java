package net.novogrodsky;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ParseInputFile {


    @Getter
    @Setter
    int rows = 0;

    @Getter
    @Setter
    int columns = 0;

    @Getter
    @Setter
    List<List<String>> matrix;
    @Getter
    @Setter
    ArrayList<String> searchWords = new ArrayList<>();

    /**
     * @param filePath The file path and name of input file.
     */
    public void parseFile(String filePath) {
        File file = new File(filePath);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String rowAndColumn = bufferedReader.readLine().trim();
            parseRowAndColumn(rowAndColumn);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        matrix = new ArrayList<>();

        IntStream.range(0, getRows()).forEach(i -> {
            try {
                matrix.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(String::toString)
                                .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        // Parse the last lines of the input file.
        // These lines contain the words we are seeking.
        while (true) {
            try {
                String wordToSearch = bufferedReader.readLine();
                if (wordToSearch != null) {
                    wordToSearch.replaceAll("\\s+","");
                    searchWords.add(wordToSearch);
                } else {
                    break;
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void parseRowAndColumn(String rowAndColumn) {
        String[] rowColumn = rowAndColumn.split("x");

        this.setRows(Integer.parseInt(rowColumn[0]));
        this.setColumns(Integer.parseInt(rowColumn[1]));
    }

}
