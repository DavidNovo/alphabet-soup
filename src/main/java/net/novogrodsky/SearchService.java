package net.novogrodsky;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class SearchService {

    public static int[] findWord(char[][] matrix, String word, boolean backwardSearch) {

        int numRows = matrix.length;
        int numCols = matrix[0].length;

        final String newWord;
        if (backwardSearch) {
            StringBuilder reversedString;
            reversedString = new StringBuilder(word);
            newWord = reversedString.reverse().toString();
        } else {
            newWord = word;
        }
        // Search horizontally, reading from left to right
        Optional<int[]> horizontal = Arrays.stream(matrix)
                .mapToInt(row -> new String(row).indexOf(newWord))
                .filter(index -> index != -1)
                .mapToObj(index -> new int[]{index, index + newWord.length() - 1})
                .findFirst();

        if (horizontal.isPresent()) {
            int row = IntStream.range(0, numRows)
                    .filter(i -> new String(matrix[i]).indexOf(newWord) != -1)
                    .findFirst()
                    .getAsInt();
            if (backwardSearch) {
                return new int[]{row, horizontal.get()[1], row, horizontal.get()[0]};
            } else {
                return new int[]{row, horizontal.get()[0], row, horizontal.get()[1]};
            }
        }

        // Search vertically
        Optional<int[]> vertical = IntStream.range(0, numCols)
                .mapToObj(col -> {
                    String colStr = "";
                    for (int row = 0; row < numRows; row++) {
                        colStr += matrix[row][col];
                    }
                    return colStr;
                })
                .map(str -> str.indexOf(newWord))
                .filter(index -> index != -1)
                .map(index -> new int[]{index, index + newWord.length() - 1})
                .findFirst();

        if (vertical.isPresent()) {
            int column = IntStream.range(0, numCols)
                    .filter(col -> {
                        String colStr = "";
                        for (int row = 0; row < numRows; row++) {
                            colStr += matrix[row][col];
                        }
                        return colStr.contains(newWord);
                    })
                    .findFirst()
                    .orElse(-1);

            System.out.println("column is: " + column + "  and verticals are: "
                    + vertical.get()[0] + " and " + vertical.get()[1]);
            if (backwardSearch) {
                return new int[]{vertical.get()[1], column, vertical.get()[0], column};
            } else {
                return new int[]{vertical.get()[0], column, vertical.get()[1], column};
            }
        }


        // Search diagonally (top-left to bottom-right)
        for (int i = 0; i < numRows - newWord.length() + 1; i++) {
            for (int j = 0; j < numCols - newWord.length() + 1; j++) {
                boolean found = true;
                for (int k = 0; k < newWord.length(); k++) {
                    if (matrix[i + k][j + k] != newWord.charAt(k)) {
                        found = false;
                        break;
                    }
                }
                if (found) {
                    if (backwardSearch) {
                        return new int[]{i + newWord.length() - 1, j + newWord.length() - 1, i, j};
                    } else {
                        return new int[]{i, j, i + newWord.length() - 1, j + newWord.length() - 1};
                    }
                }
            }
        }

        // Search diagonally (bottom-left to top-right)
        for (int i = newWord.length() - 1; i < numRows; i++) {
            for (int j = 0; j < numCols - newWord.length() + 1; j++) {
                boolean found = true;
                for (int k = 0; k < newWord.length(); k++) {
                    if (matrix[i - k][j + k] != newWord.charAt(k)) {
                        found = false;
                        break;
                    }
                }
                if (found) {
                    if (backwardSearch) {
                        return new int[]{i - newWord.length() + 1, j + newWord.length() - 1,i, j };
                    } else {
                        return new int[]{i, j, i - newWord.length() + 1, j + newWord.length() - 1};
                    }

                }
            }
        }
        // if word not found
        return null;
    }

    public static char[][] changeToArray(List<List<String>> matrix) {
        int numRows = matrix.size();
        int numCols = matrix.get(0).size();

        char[][] charMatrix = new char[numRows][numCols];

        for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
            List<String> row = matrix.get(rowIndex);
            for (int columnIndex = 0; columnIndex < numCols; columnIndex++) {
                String cell = row.get(columnIndex);
                charMatrix[rowIndex][columnIndex] = cell.charAt(0);  //assume each cell is a single character
            }
        }
        return charMatrix;
    }
}
