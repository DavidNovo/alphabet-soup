package net.novogrodsky;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter full file path to input file, for example src/test/resources/largeTest.txt:");
        String filePath = in.nextLine();
        ParseInputFile parseInputFile = new ParseInputFile();
        parseInputFile.parseFile(filePath);

        char[][] matrixSearched = SearchService.changeToArray(parseInputFile.getMatrix());
        ArrayList<String> searchWords = parseInputFile.getSearchWords();

        for (String word : searchWords) {
            int[] location;
            location = SearchService.findWord(matrixSearched, word, false);
            if (location != null) {
                String output = String.format("%s %d:%d %d:%d", word, location[0], location[1], location[2], location[3]);
                System.out.println(output);
            } else {
                location = SearchService.findWord(matrixSearched, word, true);
                String output = String.format("%s %d:%d %d:%d", word, location[0], location[1], location[2], location[3]);
                System.out.println(output);
            }

        }
        System.exit(0);
    }
}
